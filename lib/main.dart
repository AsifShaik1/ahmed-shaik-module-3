/*
  Create 3 .dart files for the exercise. All code outputs are printed on the 
  console.

  Create a Flutter app that has the following screens, navigating to each other. 
  Your app must have up to 6 screens (login, dashboard, 2 feature screens, and
  user profile edit):

  - Login and registration screens (not linked to a database) with relevant 
   input fields.
  - Dashboard (the screen after login) with buttons to feature screens of your
  app.
  - The last screen must be of a user profile edit.
  - Dashboard Floating button on the home screen, linking to another screen.
  - Each screen must be labeled appropriately on the app bar.
*/

import 'package:flutter/material.dart';
import 'login.dart';
import 'profile.dart';
import 'dashboard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'MTN App Module 3 Code';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const LoginPage(),
        //body: const Profile()
        //body: const Dashboard(),
      ),
    );
  }
}
