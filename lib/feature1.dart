import 'package:flutter/material.dart';
import 'dashboard.dart';

class Feature1 extends StatefulWidget {
  const Feature1({Key? key}) : super(key: key);

  @override
  State<Feature1> createState() => _Feature1();
}

class _Feature1 extends State<Feature1> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Feature 1")),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('feature1.jpg', width: 200, fit: BoxFit.fitWidth),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Dashboard()),
                  );
                },
                child: const Text('Goto Dashboard'),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Dashboard()),
          );
        },
        child: const Icon(Icons.home),
      ),
    );
  }
}
